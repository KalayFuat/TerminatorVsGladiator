﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public float health = 1.0f;
    public float moveSpeed = 10.5f;
    public float jiggleAmount = 1f;
    public float jiggleTime = 0.5f;
    public int maxJumpCount = 2;

    // Start is called before the first frame update
    public Rigidbody rb;

    private KeyCode KeyLeft = KeyCode.A;
    private KeyCode KeyRight = KeyCode.D;
    private KeyCode KeyJump = KeyCode.W;
    private KeyCode KeyFire = KeyCode.Space;

    private int hasJumped = 0;

    private MaterialPropertyBlock materialBlock;
    private Renderer rend;
    Color colourOriginal;
    Color colourSeverlyDamaged;
    Color colourCurrent;

    GameObject thisObject;
    Player thisPlayer;
    //BulletController bulletController;

    private float time = 0.0f;

    void Start()
    {
        rend = GetComponent<Renderer>();

        colourOriginal = rend.material.color;
        colourSeverlyDamaged = Color.red;
        
        rb = GetComponent<Rigidbody>();
        thisObject = GameObject.Find(this.name);
        thisPlayer = thisObject.GetComponent<Player>();

        materialBlock = new MaterialPropertyBlock();

       // bulletController = GameObject.Find("BulletShooter").GetComponent<BulletController>();
        //bulletController.transform.position = new Vector3(this.gameObject.transform.position.x, this.gameObject.transform.position.y, this.gameObject.transform.position.z);
    }

    void Update()
    {
        //isJumpPressed = Input.GetKeyDown(KeyCode.W);
        //isLeftPressed = Input.GetKey(KeyCode.A);
        //isRightPressed = Input.GetKey(KeyCode.D);


       // bulletController.transform.position = thisPlayer.transform.position;

        DoKeyEvents();

        
    }

    void OnGUI()
    {
        //GUILayout.Label((KeyLeft == KeyCode.A).ToString());


    }

    void FixedUpdate()
    {
        if (rb.velocity.y < 0)
        {
            FallFaster();
        }

    }

    private void TenSeconds()
    {
        time = time + Time.fixedDeltaTime;
        if (time > 10.0f)
        {
            time = 0.0f;
        }
    }

    public void AssignKeys(KeyCode left, KeyCode right, KeyCode jump, KeyCode fire)
    {
        KeyLeft = left;
        KeyRight = right;
        KeyJump = jump;
        KeyFire = fire;
    }

    public void DoKeyEvents()
    {
        DoKeyEvents(KeyJump, Input.GetKeyDown(KeyJump));
        DoKeyEvents(KeyLeft, Input.GetKey(KeyLeft));
        DoKeyEvents(KeyRight, Input.GetKey(KeyRight));
        DoKeyEvents(KeyFire, Input.GetKey(KeyFire));
    }

   // public void DoKeyEvents(bool isJumpPressed, bool isLeftPressed, bool isRightPressed, bool isFirePressed)
    public void DoKeyEvents(KeyCode key, bool isPressed)
    {
        //Debug.Log(key.ToString());

        if (key == KeyJump && isPressed)
        {
            Debug.Log("key JUMP");
            if (hasJumped < maxJumpCount)
            {
                Jump();
                hasJumped++;
            }
        }

        else if (key == KeyLeft && isPressed)
        {
            Debug.Log("key left");
            //rb.velocity = new Vector3(0, rb.velocity.y, rb.velocity.z);
            rb.AddForce(-moveSpeed, 0, 0, ForceMode.VelocityChange);
        }

        else if (key == KeyRight && isPressed)
        {
            Debug.Log("key right");
            //rb.velocity = new Vector3(0, rb.velocity.y, rb.velocity.z);
            rb.AddForce(moveSpeed, 0, 0, ForceMode.VelocityChange);
        }

        else if(key == KeyFire && isPressed)
        {
            Debug.Log("key fire");
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        //dont move in the z direction
        //rb.velocity = new Vector3(rb.velocity.x, rb.velocity.y, 0);

        if (collision.gameObject.name == "GroundPlane")
        {
            //Debug.Log("Touched Ground");
            if (Input.GetKey(KeyJump))
            {
                Debug.Log("welkjfhadsgsd,bfsgfsrfd");
                Jump();
                //hasJumped--;
            }
            else
                hasJumped = 0;
        }

        else if(collision.gameObject.tag == "Platform")
        {
            hasJumped = 0;
        }

        else if(collision.gameObject.name.StartsWith("Player_"))
        {
            PlayerHit(collision);
        }

        else if(collision.gameObject.name == "Bullet")
        {
            //Physics.IgnoreCollision(thisObject.GetComponent<Collider>(), collision.collider);
        }

        else if (collision.gameObject.name.StartsWith("Bullet"))
        {
            PlayerHit(collision);
        }
    }

    public void FallFaster()
    {
        //Debug.Log("FallFaster");
        rb.AddForce(0, -10.0f, 0, ForceMode.VelocityChange);

        //rb.AddForce(0, -100.0f, 0, ForceMode.Acceleration);
    }

    private void StopJump()
    {
        rb.velocity = new Vector3(0, 0, 0);
    }

    private void Jump()
    {
        //rb.velocity = new Vector3(0, 0, 0);
        rb.AddForce(0, 40.0f, 0, ForceMode.Impulse);
        //rb.AddForce(0, -800.0f, 0, ForceMode.Acceleration);
        //Jiggle();
    }

    private void Bounce()
    {
        rb.velocity = new Vector3(0, 80, 0);
    }

    private void BounceSmall()
    {
        rb.velocity = new Vector3(0, 10, 0);
    }

    protected void PlayerHit(Collision collision)
    {
        health -= 0.1f;
        if (health <= 0)
            health = 1.0f;

        colourCurrent = Color.Lerp(colourSeverlyDamaged, colourOriginal, health);
        materialBlock.SetColor("_Color", colourCurrent);
        rend.SetPropertyBlock(materialBlock);

       // Jiggle();
    }

    protected void Jiggle()
    {
        iTween.PunchScale(thisObject, new Vector3(jiggleAmount, jiggleAmount, jiggleAmount), jiggleTime);
    }

    public void FireBullet(Bullet bullet)
    {

    }
}

//if (isMoving && hasJumped)
//{
//    // when the cube has moved for 10 seconds, report its position
//    time = time + Time.fixedDeltaTime;
//    if (time > 10.0f)
//    {
//        Debug.Log(gameObject.transform.position.y + " : " + time);
//        time = 0.0f;
//    }
//}


//if (isJumpPressed)
//{
//    // the cube is going to move upwards in 10 units per second
//    rb.velocity = new Vector3(0, 20, 0);
//    hasJumped = true;
//    Debug.Log("jump");
//}

//if (hasJumped)
//{
//    Debug.Log("has jumped");
//    // when the cube has moved for 10 seconds, report its position
//    time = time + Time.fixedDeltaTime;
//    if (time > 10.0f)
//    {
//        Debug.Log(gameObject.transform.position.y + " : DSFGDFGDFG " + time);
//        time = 0.0f;
//    }

//    Transform tGround = Ground.transform;
//    if (transform.position.y <= transform.position.y)
//    {
//        rb.velocity = new Vector3(0, 0, 0);
//        hasJumped = false;
//    }
//}


//Vector3 position = this.transform.position;
//position.x -= moveSpeed;
//this.transform.position = position;